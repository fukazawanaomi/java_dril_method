

public class Practice {

	static double getTriangleArea(double height, double base) {
		return base * height / 2;
	}

	public static void main(String[] args) {
		double triangleArea = getTriangleArea(8.5, 7.2);
		System.out.println(triangleArea);
	}

}

package java_dril_method;

public class Practice12 {
	static String getWeatherForecast(String weather) {
		return weather;
	}

	public static void main(String[] args) {
		String[] day = {"今日", "明日", "明後日"};
		String[] weatherrand = {"晴れ", "曇り", "雨", "雪"};
		int n = (int) (3 * Math.random());
		int w = (int) (4 * Math.random());
		System.out.println(day[n] + "の天気は" + getWeatherForecast(weatherrand[w]) + "でしょう。");
	}

}

package java_dril_method;

public class Practice17 {
	static double getMinValue(double a, double b) {
		if(a < b) {
			return a;
		} else {
			return b;
		}
	}

	public static void main(String[] args) {
		System.out.println(getMinValue(4.5, 3.4));

	}

}

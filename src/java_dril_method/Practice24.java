package java_dril_method;

class Point2 {
	 double x;
	 double y;

	 Point2(double x, double y) {
	 this.x = x;
	 this.y = y;
	 }
}

public class Practice24 {

	static double getDistanceBetweenTwoPoints(Point2 p0, Point2 p1) {
		return Math.sqrt((p0.x - p1.x)*(p0.x -p1.x)+(p0.y - p1.y)*(p0.y - p1.y));
	}

	public static void main(String[] args) {
		Point2 p0 = new Point2(3.2, 4.5);
		Point2 p1 = new Point2(7.8, 1.0);

		System.out.println(getDistanceBetweenTwoPoints(p0, p1));
	}

}

package java_dril_method;

public class Practice20 {
	static int getMinValue(int[] array) {
		int min = array[0];
		for(int i = 1; i < array.length; i++) {
			if(min > array[i]) {
				min = array[i];
			}
		}
		return min;
	}

	public static void main(String[] args) {
		int list[] = {2,7,2,5,9,0,3,1};
		System.out.println(getMinValue(list));
		}
	}

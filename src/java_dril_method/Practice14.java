package java_dril_method;

public class Practice14 {
	static String getRandomMessage(String name) {
		return name;
	}

	public static void main(String[] args) {
		String[] hello = {"こんばんは" + getRandomMessage("××") + "さん", "こんにちは" + getRandomMessage("××") + "さん", "おはよう" + getRandomMessage("××") + "さん"};
		int n = (int)(3 * Math.random());
		System.out.println(hello[n]);
	}

}

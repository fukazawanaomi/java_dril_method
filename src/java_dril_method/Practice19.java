package java_dril_method;

public class Practice19 {
	static String getMessage(String name, boolean iskids) {
		if(iskids == true) {
			return "こんにちは。" + name + "ちゃん。";
		} else {
			return "こんにちは。" + name + "さん。";
		}
	}

	public static void main(String[] args) {
		System.out.println(getMessage("xx", false));
	}

}

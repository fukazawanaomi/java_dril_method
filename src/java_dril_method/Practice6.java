package java_dril_method;

public class Practice6 {

	static void printCircleArea(double radius) {
		System.out.println(radius * radius * Math.PI);
	}

	public static void main(String[] args) {
		printCircleArea(2.0);
	}

}

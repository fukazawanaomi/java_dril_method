package java_dril_method;

public class Practice13 {
	static double getSquareRootOf2(double b) {
		return Math.sqrt(b);
	}

	public static void main(String[] args) {
		System.out.println(getSquareRootOf2(2.0));
	}

}

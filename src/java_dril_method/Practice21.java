package java_dril_method;

public class Practice21 {
	static double getAverage(double[] array) {
		double avg = 0;
		for(int i = 0; i < array.length; i++) {
			avg += array[i];
		}
		return avg / array.length;
	}

	public static void main(String[] args) {
		double[] list = {7.3, 2.2, 8, 4.5, 1.0, 9.9};
		System.out.println(getAverage(list));
	}

}

package java_dril_method;

class Point {
	double x;
	double y;

	Point(double x, double y) {
		this.x = x;
		this.y = y;
	}
}

public class Practice23 {

	static double getDistanceFromOrigin(Point p) {
		return Math.sqrt(p.x * p.x + p.y * p.y);
	}

	public static void main(String[] args) {
		Point p1 = new Point(2.2, 6.4);

		System.out.println(getDistanceFromOrigin(p1));
	}
}
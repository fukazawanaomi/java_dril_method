package java_dril_method;

public class Practice22 {
	static String getLongestString(String[] array) {
		String s = array[0];
		for(int i = 1; i < array.length; i++) {
			if(s.length() == array[i].length()) {
				s = array[i];
			}else if(s.length() < array[i].length()) {
				s = array[i];
			}
		}
		return s;
	}

	public static void main(String[] args) {
		String[] str = {"こんにちは", "364662", "はじめまして、深澤です", "４７８３８８８８８３", "はじめまして、山田です"};
		System.out.println(getLongestString(str));
	}

}

package java_dril_method;

public class Practice10 {
	static void printMaxValue(double a, double b, double c) {
		if(a > b && a > c) {
			System.out.println(a);
		}
		if(b > a && b > c) {
			System.out.println(b);
		}
		if(c > a && c > b) {
			System.out.println(c);
		}
	}

	public static void main(String[] args) {
		printMaxValue(2.4, 5.5, 3.2);
	}

}
